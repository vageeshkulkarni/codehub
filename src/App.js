// import logo from './logo.svg';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './App.css';
import Header from './Components/Layout/Header/Header';
import Hero from './Components/Layout/HeroSection/Hero';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap-icons/font/bootstrap-icons.css';

function App() {
  // return (
  //   <div className="App">
  //     <header className="App-header">
  //       <img src={logo} className="App-logo" alt="logo" />
  //       <p>
  //         Edit <code>src/App.js</code> and save to reload.
  //       </p>
  //       <a
  //         className="App-link"
  //         href="https://reactjs.org"
  //         target="_blank"
  //         rel="noopener noreferrer"
  //       >
  //         Learn React
  //       </a>
  //     </header>
  //   </div>
  // );

  return (
    <BrowserRouter>
      <Header />
      <Hero />
      {/* <Slider />
      <Routes>
        <Route path='/' element={<Users headingText="User List"/>} />
        <Route path='users' element={<Users headingText="User List"/>} />
        <Route path='about' element={<About />}/>
        <Route path='mobiles' element={<Mobiles />}/>
        <Route path='product-details' element={<Product />} />
        <Route path='cart' element={<Cart />} />
        <Route path="use-context-example" element={<Parent />}/>
        <Route path="use-reducer-example" element={<UseReducer />}/>
        <Route path='login' element={<Login/>}/>
      </Routes>
      <Footer /> */}
    </BrowserRouter>
  );
}

export default App;
